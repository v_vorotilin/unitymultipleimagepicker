//
//  ObjCWrapperUnityMultipleImagePicker.h
//  UnityMultipleImagePicker
//
//  Created by vadim on 7/4/16.
//  Copyright © 2016 Vadim Vorotilin. All rights reserved.
//

#ifndef ObjCWrapperUnityMultipleImagePicker_h
#define ObjCWrapperUnityMultipleImagePicker_h

#import <UIKit/UIKit.h>
#import "AGImagePickerController/AGImagePickerController.h"

@interface ObjCWrapperImagePickerCaller : NSObject <AGImagePickerControllerDelegate>

typedef void (^Callback)(const char* url, const char* value);

@property (nonatomic) int itemsInRowForIPadLandscape;
@property (nonatomic) int itemsInRowForIPadPortrait;
@property (nonatomic) int itemsInRowForIPhoneLandscape;
@property (nonatomic) int itemsInRowForIPhonePortrait;

@property (nonatomic) int maximumSelectedPhotosCount;

@property (nonatomic, strong) UIView *pluginView;
@property (nonatomic) int status;
@property (nonatomic, strong) NSString *errorMessage;
@property (nonatomic, strong) UIViewController *unityVC;
@property (nonatomic, strong) AGImagePickerController *ipc;

@property (nonatomic, strong) NSMutableArray *photosUrls;
@property (nonatomic, strong) NSData *imageData;

- (void) callImagePicker;
- (NSString*) getPhotosUrls;

- (NSString*) getErrorMessage;
- (NSString*) getImage;
- (void) requestImageByAssetUrl:(NSString*)assetUrlString;

@end

#endif /* ObjCWrapperUnityMultipleImagePicker_h */
