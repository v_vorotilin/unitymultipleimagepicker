//
//  CWrapperUnityMultipleImagePicker.mm
//  UnityMultipleImagePicker
//
//  Created by vadim on 7/4/16.
//  Copyright © 2016 Vadim Vorotilin. All rights reserved.
//

#import "ObjCWrapperUnityMultipleImagePicker.h"

NSString* CreateNSString (const char* string) {
    if (string)
        return [NSString stringWithUTF8String: string];
    else
        return [NSString stringWithUTF8String: ""];
}

char* MakeStringCopy (const char* string) {
    if (string == NULL)
        return NULL;
    
    char* res = (char*) malloc(strlen(string) + 1);
    strcpy(res, string);
    return res;
}

char* MakeStringCopy (NSString* string) {
    return MakeStringCopy([string UTF8String]);
}

ObjCWrapperImagePickerCaller *caller;

void createCallerIfNil() {
    if (!caller)
        caller = [[ObjCWrapperImagePickerCaller alloc] init];
}

extern "C" {
    
    void _callImagePicker() {
        createCallerIfNil();
        
        [caller callImagePicker];
    }
    
    int _getStatus() {
        if (!caller)
            return -1;
        
        return caller.status;
    }
    
    const char* _getImagesPaths() {
        if (!caller)
            return MakeStringCopy(new char[0]);
        
        return MakeStringCopy([caller getPhotosUrls]);
    }
    
    const char* _getErrorMessage() {
        if (!caller)
            return MakeStringCopy(new char[0]);
        
        return MakeStringCopy([caller getErrorMessage]);
    }
    
    const char* _getImage() {
        if (!caller)
            return MakeStringCopy(new char[0]);
        
        return MakeStringCopy([caller getImage]);
    }
    
    void _requestImageByAssetUrl(const char* assetUrl) {
        createCallerIfNil();
        
        [caller requestImageByAssetUrl:CreateNSString(assetUrl)];
    }
    
    void _setMaximumSelection(int value) {
        createCallerIfNil();
        
        caller.maximumSelectedPhotosCount = value;
    }
    
    int _getMaximumSelection() {
        if (!caller)
            return -1;
        
        return caller.maximumSelectedPhotosCount;
    }
    
}
