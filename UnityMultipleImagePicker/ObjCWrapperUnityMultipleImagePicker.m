//
//  ObjCWrapperUnityMultipleImagePicker.m
//  UnityMultipleImagePicker
//
//  Created by vadim on 7/4/16.
//  Copyright © 2016 Vadim Vorotilin. All rights reserved.
//

#import "ObjCWrapperUnityMultipleImagePicker.h"
#import "AGImagePickerController/AGIPCToolbarItem.h"

@import Photos;

@implementation ObjCWrapperImagePickerCaller

-(ObjCWrapperImagePickerCaller*) init {
    if (!(self = [super init]))
        return nil;
    
    self.photosUrls = [[NSMutableArray alloc] init];
    
    self.unityVC = [[UIApplication sharedApplication] keyWindow].rootViewController;
    
    self.status = 0;
    
    self.itemsInRowForIPadLandscape = 7;
    self.itemsInRowForIPadPortrait = 6;
    self.itemsInRowForIPhoneLandscape = 5;
    self.itemsInRowForIPhonePortrait = 4;
    
    self.ipc = [[AGImagePickerController alloc] initWithDelegate:self];
    self.ipc.shouldShowSavedPhotosOnTop = NO;
    self.ipc.shouldChangeStatusBarStyle = YES;
    
    // Custom toolbar items
    AGIPCToolbarItem *selectAll = [[AGIPCToolbarItem alloc] initWithBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Select All" style:UIBarButtonItemStyleBordered target:nil action:nil] andSelectionBlock:^BOOL(NSUInteger index, ALAsset *asset) {
        return YES;
    }];
    
    AGIPCToolbarItem *flexible = [[AGIPCToolbarItem alloc] initWithBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] andSelectionBlock:nil];
    
    AGIPCToolbarItem *deselectAll = [[AGIPCToolbarItem alloc] initWithBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Deselect All" style:UIBarButtonItemStyleBordered target:nil action:nil] andSelectionBlock:^BOOL(NSUInteger index, ALAsset *asset) {
        return NO;
    }];
    
    self.ipc.toolbarItemsForManagingTheSelection = @[selectAll, flexible, deselectAll];
    
    return self;
}

#pragma mark Implementation

-(void) callImagePicker {
    self.ipc.maximumNumberOfPhotosToBeSelected = self.maximumSelectedPhotosCount;
    self.unityVC.view.exclusiveTouch = NO;
    
    self.status = 1;
    
    [self.unityVC presentViewController:self.ipc animated:YES completion:NULL];
}

- (NSString*) getPhotosUrls {
    NSString* result = [[NSString alloc] init];
    
    for (NSString* url in self.photosUrls) {
        result = [NSString stringWithFormat:@"%@\n%@", result, url];
    }
    
    return result;
}

- (NSString*) getErrorMessage {
    return self.errorMessage;
}

- (NSString*) getImage {
    NSString* base64Image = [self.imageData base64Encoding];
    self.imageData = nil;
    
    return base64Image;
}

- (void) requestImageByAssetUrl:(NSString*)assetUrlString {
    NSURL* assetUrl = [[NSURL alloc] initWithString:assetUrlString];
    
    if (assetUrl == nil) {
        self.imageData = nil;
        self.errorMessage = [NSString stringWithFormat:@"Couldn't parse URL: %@", assetUrlString];
        self.status = -1;
        
        return;
    }
    
    PHFetchResult<PHAsset*>* fetchResult = [PHAsset fetchAssetsWithALAssetURLs:[NSArray arrayWithObject:assetUrl]
                                                                       options:nil];
    
    if (fetchResult == nil || fetchResult.count == 0) {
        self.imageData = nil;
        self.errorMessage = [NSString stringWithFormat:@"Couldn't fetch the asset by URL: %@", assetUrlString];
        self.status = -1;
        
        return;
    }
    
    self.status = 1;
    
    PHAsset* asset = fetchResult.firstObject;
    [[PHImageManager defaultManager] requestImageDataForAsset:asset
                                                      options:nil
                                                resultHandler:^(NSData *imageData,
                                                               NSString *dataUTI,
                                                               UIImageOrientation orientation,
                                                               NSDictionary *info) {
                                                    self.imageData = imageData;
                                                    self.status = 0;
    }];
}

#pragma mark Image picker controller delegate

- (void)agImagePickerController:(AGImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    NSLog(@"Assets --> %@", info);
    
    [self.photosUrls removeAllObjects];
    
    for (ALAsset* asset in info)
    {
        NSString* url = asset.defaultRepresentation.url.absoluteString;
        //NSDictionary* metadata = asset.defaultRepresentation.metadata;
        
        [self.photosUrls addObject:url];
    }
    
    [self.unityVC dismissViewControllerAnimated:YES completion:NULL];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    
    self.status = 0;
}

- (void)agImagePickerController:(AGImagePickerController *)picker didFail:(NSError *)error
{
    NSLog(@"Fail --> %@", error);
    
    [self.photosUrls removeAllObjects];
    
    if (error == nil)
    {
        self.status = 2;
        
        [self.unityVC dismissViewControllerAnimated:YES completion:NULL];
    }
    else
    {
        self.status = -1;
        
        // We need to wait for the view controller to appear first.
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self.unityVC dismissViewControllerAnimated:YES completion:NULL];
        });
    }
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
}

@end