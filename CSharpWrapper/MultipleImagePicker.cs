using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

public class MultipleImagePicker : MonoBehaviour {

    private delegate void Callback(string url, string value);

    [DllImport("__Internal")]
    private static extern void _callImagePicker();

    [DllImport("__Internal")]
    private static extern int _getStatus();

    [DllImport("__Internal")]
    private static extern string _getErrorMessage();

    [DllImport("__Internal")]
    private static extern string _getImage();

    [DllImport("__Internal")]
    private static extern string _getImagesPaths();

    [DllImport("__Internal")]
    private static extern void _requestImageByAssetUrl(string assetUrl);

    [DllImport("__Internal")]
    private static extern void _setMaximumSelection(int value);

    [DllImport("__Internal")]
    private static extern int _getMaximumSelection();

    private static MultipleImagePicker _instance;

    private enum LocalStatus { None, Picking, LoadingAsset };

    private LocalStatus _localStatus = LocalStatus.None;

    public static int MaximumSelection {
        get { return _getMaximumSelection(); }
        set { _setMaximumSelection(value); }
    }

    void Awake() {
        if (_instance != null)
            throw new InvalidOperationException(string.Format("More than one instance of MultipleImagePicker created"));

        _instance = this;

        DontDestroyOnLoad(this);
    }

    void Update() {
        if (Application.platform != RuntimePlatform.IPhonePlayer)
            return;

        if (_localStatus == LocalStatus.None)
            return;

        int status = _getStatus();

        if (_localStatus == LocalStatus.Picking) {
            switch (status) {
                case -1:
                    OnError(string.Format("C wrapper returned status: {0}", status));
                    break;
                case 0:
                    string paths = _getImagesPaths();

                    Debug.LogFormat("Returned path: '{0}'", paths);

                    string[] splitted = paths.Split('\n')
                                             .Select(s => s.Trim())
                                             .Where(s => !string.IsNullOrEmpty(s))
                                             .ToArray();

                    OnSelectionFinished(splitted);
                    break;
                case 1:
                    break;
                case 2:
                    OnCancel();
                    break;
            }
        } else if (_localStatus == LocalStatus.LoadingAsset) {
            switch (status) {
                case -1:
                    OnImageGetError(_getErrorMessage());
                    break;
                case 0:
                    OnImageGot(_getImage());
                    break;
                case 1:
                    break;
            }
        }
    }

    private Action<string[]> onSelectionFinished;
    private Action onCancel;
    private Action<string> onError;

    public static void Show(Action<string[]> onSelectionFinished, Action onCancel, Action<string> onError) {
        if (_instance._localStatus != LocalStatus.None)
            throw new InvalidOperationException(string.Format("Picker is in status {0}. Only 1 operation in a time is supported."));

        _instance._localStatus = LocalStatus.Picking;

        _instance.onSelectionFinished = onSelectionFinished;
        _instance.onCancel = onCancel;
        _instance.onError = onError;

        if (Application.platform != RuntimePlatform.IPhonePlayer) {
            _instance.OnSelectionFinished(new string[0]);
            return;
        }

        _callImagePicker();
    }

    private void OnSelectionFinished(string[] paths) {
        _localStatus = LocalStatus.None;

        if (onSelectionFinished != null)
            onSelectionFinished(paths);
    }

    private void OnCancel() {
        _localStatus = LocalStatus.None;

        if (onCancel != null)
            onCancel();
    }

    private void OnError(string message) {
        _localStatus = LocalStatus.None;

        if (onError != null)
            onError(message);
    }

    private void OnImageGot(string imageBytesString) {
        _localStatus = LocalStatus.None;

        byte[] bytes = Convert.FromBase64String(imageBytesString);

        if (getImageCallback != null)
            getImageCallback(bytes);
    }

    private void OnImageGetError(string errorMessage) {
        _localStatus = LocalStatus.None;

        if (getImageOnError != null)
            getImageOnError(errorMessage);
    }

    private Action<byte[]> getImageCallback;
    private Action<string> getImageOnError;

    public static void GetImageByPath(string path, Action<byte[]> callback, Action<string> onError) {
        if (_instance._localStatus != LocalStatus.None)
            throw new InvalidOperationException(string.Format("Picker is in status {0}. Only 1 operation in a time is supported."));

        _instance._localStatus = LocalStatus.LoadingAsset;

        _instance.getImageCallback = callback;
        _instance.getImageOnError = onError;

        _requestImageByAssetUrl(path);
    }
}
