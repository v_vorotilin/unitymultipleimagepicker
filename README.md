Downloads
====================================================

You'll need only to download __plugin.zip__ archive. You have 2 options:

* [Stable version](https://bitbucket.org/v_vorotilin/unitymultipleimagepicker/raw/master/plugin.zip)

    Tested stable version from _master_ branch

* [Most recent version](https://bitbucket.org/v_vorotilin/unitymultipleimagepicker/raw/develop/plugin.zip)

    The most recent but not meticulously tested version from _develop_ branch. __MAY CONTAIN BUGS!!!__

Unity integration guide
====================================================

Simply merge _Assets_ folder inside [__plugin.zip__](https://bitbucket.org/v_vorotilin/unitymultipleimagepicker/raw/master/plugin.zip) archive with your Unity _Assets_ folder.

Plugin folder tree inside Unity _Assets_ folder
----------------------------------------------------

```
Assets
├── Plugins
│   ├── MultipleImagePicker
│   │   └── MultipleImagePicker.cs
│   └── iOS
│       └── MultipleImagePicker
│           ├── AGImagePickerController
│           │   ├── AGIPCAlbumsController.h
│           │   ├── AGIPCAlbumsController.m
│           │   ├── AGIPCAssetsController.h
│           │   ├── AGIPCAssetsController.m
│           │   ├── AGIPCGridCell.h
│           │   ├── AGIPCGridCell.m
│           │   ├── AGIPCGridItem.h
│           │   ├── AGIPCGridItem.m
│           │   ├── AGIPCToolbarItem.h
│           │   ├── AGIPCToolbarItem.m
│           │   ├── AGImagePickerController+Helper.h
│           │   ├── AGImagePickerController+Helper.m
│           │   ├── AGImagePickerController.h
│           │   ├── AGImagePickerController.m
│           │   ├── AGImagePickerControllerDefines.h
│           │   ├── ALAsset+AGIPC.h
│           │   └── ALAsset+AGIPC.m
│           ├── CWrapperUnityMultipleImagePicker.mm
│           ├── ObjCWrapperUnityMultipleImagePicker.h
│           └── ObjCWrapperUnityMultipleImagePicker.m
└── StreamingAssets
    └── MultipleImagePicker
        └── AGIPC-Checkmark.png
```

After-build procedures in XCode
----------------------------------------------------

1. Switch __Enable Modules__ on

    * In order to do that open project _Build Settings_ in XCode
    * Select _All_ instead of _Basic_ settings
    * Search for __Enable Modules (C and Objective-C)__ setting and set it to __Yes__

2. Add _AssetsLibrary_ and _Photos_ frameworks

    * Open project _Build Phases_ in XCode
    * Open __Link Binary With Libraries__ submenu
    * Click on __+__ sign
    * In the opened menu search for the framework and click _Add_
    * Repeat the procedure for each framework (AssetsLibrary and Photos)

Usage guide
====================================================

In order to use MultipleImagePicker inside your project you need to do the next:
    * Create an empty _GameObject_ on the scene
    * Add _MultipleImagePicker_ script to it

__MultipleImagePicker__ class documentation
----------------------------------------------------

__MultipleImagePicker__ contains 2 public static methods that are expected to be used by the developer:

| Signature | Description |
| --------- | ----------- |
| `void Show(Action<string[]> onSelectionFinished, Action onCancel, Action<string> onError)` | Shows a native multiple image picker controller and calls an appropriate callback |
| `void GetImageByPath(string path, Action<byte[]> callback, Action<string> onError)` | Requests the native API to get an image asset by its path and calls an appropriate callback |

---------------------------

__Show__ method arguments description:

| Argument                                  | Description                                                           |
| ----------------------------------------- | --------------------------------------------------------------------- |
| `Action<string[]> onSelectionFinished`    | Callback that returns selected images paths as strings in an array    |
| `Action onCancel`                         | Callback that is called when the user hits _Cancel_ button            |
| `Action<string> onError`                  | Callback that returns an error message                                |

---------------------------

__GetImageByPath__ method arguments description:

| Argument                                  | Description                                                           |
| ----------------------------------------- | --------------------------------------------------------------------- |
| `string path`    | Path of the image (should be one of the returned by __Show__'s _onSelectionFinished_ callback )|
| `Action<byte[]> callback`                 | Callback that returns image bytes                                     |
| `Action<string> onError`                  | Callback that returns an error message                                |

---------------------------

Also __MultipleImagePicker__ contains 1 public static property that can be used for customization purposes:

| Property               | Description                                                           |
| ---------------------- | --------------------------------------------------------------------- |
| `int MaximumSelection` | Gets for sets the maximum amount of photos to be selected by the user |
