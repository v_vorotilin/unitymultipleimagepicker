#!/bin/sh

echo "Preparing to copy sources from $1 to $2..."

pluginFolderName="MultipleImagePicker"

sourcesRoot=$1
targetDirectory=$2

# CSharp wrapper
echo "Copying wrapper..."

ditto -v $sourcesRoot/CSharpWrapper/MultipleImagePicker.cs $targetDirectory/Assets/Plugins/$pluginFolderName/

# Native code
echo "Copying native code..."

sources=( "CWrapperUnityMultipleImagePicker.mm" "ObjCWrapperUnityMultipleImagePicker.h" "ObjCWrapperUnityMultipleImagePicker.m" )

iosPluginsFolder=$targetDirectory/Assets/Plugins/iOS

for src in "${sources[@]}"
do
    ditto -v $sourcesRoot/UnityMultipleImagePicker/$src $iosPluginsFolder/$pluginFolderName/
done

ditto -v $sourcesRoot/UnityMultipleImagePicker/AGImagePickerController/* $iosPluginsFolder/$pluginFolderName/AGImagePickerController/

# Resources
echo "Copying resources..."

ditto -v $sourcesRoot/UnityMultipleImagePicker/Data/Raw/$pluginFolderName/AGIPC-Checkmark.png $targetDirectory/Assets/StreamingAssets/$pluginFolderName/

echo "Copying sources from $1 to $2 is completed!"
