#!/bin/sh

echo "Preparing to copy and zip..."

sourcesRoot=$1
version=$2
tempFolder=/tmp/$(cat /dev/random | LC_CTYPE=C tr -dc "[:alpha:]" | head -c 8)

echo "Removing previous zip..."

rm $sourcesRoot/plugin.zip

echo "Copying to temp folder..."

mkdir $tempFolder
bash "$(dirname "$0")"/copy_plugin_sources_to_directory.sh $sourcesRoot $tempFolder

echo "Creating version file (version $version)..."

cd $tempFolder
echo $version > version.txt

echo "Zipping..."

zip -r -X $sourcesRoot/plugin.zip Assets/ version.txt

echo "Deleting temp directory..."

rm -rf $tempFolder

echo "Copying and zipping completed!"
